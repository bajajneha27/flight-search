$(function(){
	$.ui.autocomplete.prototype._renderItem = function (ul, item) {
	    item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
	    return $("<li></li>")
	            .data("item.autocomplete", item)
	            .append("<a>" + item.label + "</a>")
	            .appendTo(ul);
	};

  $( ".city_autocomplete" )
    .autocomplete({
    	source: function (request, response) {
    		$.ajax({
    			 url: "/airport-codes.json",
    			 dataType: 'json',
    			 data: {term: request.term},
    			 success: function( data ) {
    			 	var query = request.term.toLowerCase(),
    			 		filtered;

    			 	filtered = $.grep(data, function(item) {
    			 	  return item['City name'].toLowerCase().indexOf(query) !==-1 ;
    			 	});

    			 	filtered = $.map( filtered, function( item ) {
    			 		result = item['City name'] + '( ' + item['Airport Code'] + ' )';
    			 		return(result)
    			 	});

    			 	response(filtered);
    			 }
    		});
    	},
    	minLength: 3,
    	delay: 0,
    	matchContains:true,
    	focus: function(event, ui) {
      	$(this).val(ui.item.value);
        return false;
      }
  });

  $('#departure_date').datepicker({"dateFormat":"yy-mm-dd"})
});

function decrementer() {
  var count = parseInt($('.adultCount').text());
  if(count == 1)
      return
  else {
      $('.adultCount').text(count - 1)
      $('.count').val(count - 1)
  }
  return
}

function incrementer() {
  var count = parseInt($('.adultCount').text());
  console.log(typeof(count))
  if (count == 10)
      return
  else {
      $('.adultCount').text(count + 1)
      $('.count').val(count + 1)
  }
}