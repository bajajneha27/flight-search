require 'json'
require 'net/http'

class SearchController < ApplicationController

	def index
	end

	def search
		from_city = params[:from_city].split()[1] if !params[:from_city].blank?
		to_city = params[:to_city].split()[1] if !params[:to_city].blank?
		adultCount = params[:adult_count].to_i if !params[:adult_count].blank?
		departure_date = params[:departure_date] if !params[:departure_date].blank?

		request = { "request" => { "passengers"=> {  "adultCount"=> "#{adultCount}" }, "slice"=> [ { "origin"=> "#{from_city}", "destination"=> "#{to_city}", "date"=> "#{departure_date}" } ] } }

		url = CONFIG.API_endpoint + "?key=" + CONFIG.google_API_key
		uri = URI.parse(url)
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		req = Net::HTTP::Post.new("#{uri.path}?key=#{CONFIG.google_API_key}&fields=trips/tripOption",{'Content-Type' =>'application/json', 'Accept-Encoding' => 'gzip', 'User-Agent' => 'gzip'})
		req.body = request.to_json

		res = http.request(req)
		sio = StringIO.new(res.body)
		gz = Zlib::GzipReader.new(sio)
		@response = JSON.parse(gz.read())
		# raise "#{@response}"

		render :action=>"index"
	end
	
end