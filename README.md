This is a sample Rails App using Google QPX Express API for Flights Search

Setup App:
1. This app runs on ruby 2.1.2 and rails version 4.2.4
2. cd app directory and bundle install.
3. rails s to start the server.

Your app will be up and running on http://localhost:3000

Type from and to city to travel and a date of travelling, click on search and get the details.